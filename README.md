# Soundcloud Bot Filter Firefox Addon

<a rel="noreferrer noopener" href="https://addons.mozilla.org/en-US/firefox/addon/soundcloud-bot-blocker/"><img alt="Firefox Add-ons" src="https://img.shields.io/badge/Firefox-141e24.svg?&style=for-the-badge&logo=firefox-browser&logoColor=white"></a>

An add-on for Firefox that filters adult content bots on Soundcloud.

## The Problem

Soundcloud has a dumb amount of adult content bots on their platform. They use inappropriate images to draw people to their pages so they will click their sketchy link. Go to any top/trending song on Soundcloud right now and view the reposters to see for yourself.

## What it does

Listens for GET requests for comments, reposters and likers and filters users with suspicious permalinks. It is simple but seems to work for 99% of the bots.

## TODO

Feel free to make a PR to make this compatible for Chrome. Chrome has and incompatible webRequest API so I don't want to deal with it right now.

## Icon Credit

Icon is from: https://icons8.com/icons/authors/PkFFO5Hb3H4e/amoghdesign
